import styles from "../styles/country.module.css"

export async function getServerSideProps(context) {
	const { country } = context.query
	const res = await fetch(`https://restcountries.eu/rest/v2/name/${country}?fullText=true`)
	const data = await res.json()
	var ndata = {}
	data[0].borders.forEach(async function getnbr(mcountry) {
		var rres = await fetch(`https://restcountries.eu/rest/v2/alpha/${mcountry}`)
		var mdata = await rres.json()
		ndata[mdata[0].name] = mdata[0].flag
	})
	return {
		props: {
			countrydata: data[0],
			neighbordata: ndata,
		},
	}
}

const Post = ({ countrydata, neighbordata }) => {
    const neigbors = [];
    neighbordata = countrydata.borders
    console.log(neighbordata);
    neighbordata.forEach((country) => {
        // neigbors.push(<img src={neighbordata[country]} />);
        console.log(country);
        neigbors.push(<img src={`https://restcountries.eu/data/${country.toLowerCase()}.svg`} />);
    })
	return (
		<div className={styles.countrydetails}>
			<div className={styles.currcountrydetails}>
				<div className={styles.currcountrynameflag}>
					<h1>{countrydata.name}</h1>
					<img src={countrydata.flag} className={styles.flag} />
				</div>
				<div className={styles.currcountrydata}>
					<p>Native Name: {countrydata.nativeName}</p>
					<p>Capital: {countrydata.capital}</p>
					<p>Population: {countrydata.population}</p>
					<p>Region: {countrydata.region}</p>
					<p>Sub-Region: {countrydata.subregion}</p>
					<p>Area: {countrydata.area}</p>
					<p>
						Country Code:{" "}
						{countrydata.callingCodes.map((code) => "+" + code + ", ")}
					</p>
					<p>
						Languages:{" "}
						{countrydata.languages.map((language) => language.name + ", ")}
					</p>
					<p>
						Currencies: {countrydata.currencies.map((curr) => curr.name + ", ")}
					</p>
					<p>TimeZones: {countrydata.timezones.map((code) => code + ", ")}</p>
				</div>
			</div>
			<div className={styles.neighborhood}>
				<h2>Neighbour Countries</h2>
				<div className={styles.neighborhoodcountries}>
					{neigbors}
				</div>
			</div>
		</div>
	)
}

export default Post
