import Head from "next/head"
import Image from "next/image"
import styles from "../styles/Home.module.css"
import { useState } from "react"
import Link from "next/link"

export async function getServerSideProps() {
	const res = await fetch("https://restcountries.eu/rest/v2/all")
	const data = await res.json()
	return {
		props: {
			countries: data,
		},
	}
}

export default function Home({ countries }) {
	function calcTime(offset) {
		var datetime = new Date(new Date().getTime() + offset * 3600 * 1000)
			.toUTCString()
			.replace(/ GMT$/, "")
		return datetime
	}
	const [searchText, setSearchText] = useState("")
	return (
		<div className={styles.container}>
			<Head>
				<title>Countries API</title>
				<meta name='description' content='Web app assignment for SYMB Tech' />
				<link rel='icon' href='/favicon.ico' />
			</Head>
			<div className={styles.title}>Countries</div>
			<input
				type='text'
				placeholder='Search countries'
				className={styles.searchbar}
				onChange={(e) => {
					setSearchText(e.target.value)
				}}
			/>
			<div className={styles.countries}>
				{countries
					.filter((country) => {
						if (searchText == "") return country
						else if (
							// country.name.toLowerCase().includes(searchText.toLowerCase())
							country.name.toLowerCase().startsWith(searchText.toLowerCase())
						)
							return country
					})
					.map((country) => {
						return (
							<div className={styles.country}>
								<img src={country.flag} className={styles.flag} />
								<div className={styles.countrydetails}>
									<div className={styles.countryname}>{country.name}</div>
									<div className={styles.countrycurrency}>
										Currency:{" "}
										{country.currencies.map((curr) => curr.name + ", ")}
									</div>
									<div className={styles.countrydatetime}>
										Current date and time:{" "}
										{country.timezones.map((offs) => offs + ", ")}
									</div>
									<div>
										<a
											href={`https://www.google.com/maps/search/?api=1&query=${country.latlng[0]},${country.latlng[1]}`}
											target='_blank'>
											<button type='button' className={styles.button}>
												Show Map
											</button>
										</a>
										<Link href={`/${country.name}`}>
											<a>
												<button type='button' className={styles.button}>
													Details
												</button>
											</a>
										</Link>
									</div>
								</div>
							</div>
						)
					})}
			</div>
		</div>
	)
}
